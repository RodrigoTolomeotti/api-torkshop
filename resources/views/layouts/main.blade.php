@stack('header')

@stack('styles')

<html lang="pt-br" dir="ltr">
    <head>
        <script>

            var store = {
                apiToken: localStorage.apiToken,
                baseUrl: '{{ url() }}',
                apiUrl: '{{ url('api') }}',
                alerts: []
            }

            if (!localStorage.apiToken || localStorage.apiToken == '') {
                window.location = store.baseUrl + '/login'
            }

            axios.defaults.headers.common = {
                'Authorization': "Bearer " + localStorage.apiToken
            }

            axios.interceptors.response.use(function (response) {
                // Do something with response data
                return response;
            }, function (error) {
                if (error.response.status == 401) {
                    window.location = store.baseUrl + '/login'
                }
                return error;
            });

        </script>
    </head>
</html>
