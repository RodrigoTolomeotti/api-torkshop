<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('CoresSeeder');
        $this->call('TamanhoSeeder');
        $this->call('StatusPedidoSeeder');
        $this->call('PagamentoSeeder');
    }
}
