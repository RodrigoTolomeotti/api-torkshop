<?php

use Illuminate\Database\Seeder;
use App\Pagamento;

class PagamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pagamento::create(['descricao' => 'Boleto Bancário']);
        Pagamento::create(['descricao' => 'Cartão de Débito']);
        Pagamento::create(['descricao' => 'Cartão de Crédito']);
        Pagamento::create(['descricao' => 'Tranferência Bancária']);
    }
}
