<?php

use Illuminate\Database\Seeder;
use App\StatusPedido;

class StatusPedidoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StatusPedido::create(['descricao' => 'Pedido realizado']);
        StatusPedido::create(['descricao' => 'Aguardando pagamento']);
        StatusPedido::create(['descricao' => 'Pagamento confirmado']);
        StatusPedido::create(['descricao' => 'Pagamento recusado']);
        StatusPedido::create(['descricao' => 'Em separação']);
        StatusPedido::create(['descricao' => 'Pedido despachado']);
        StatusPedido::create(['descricao' => 'Em transporte']);
        StatusPedido::create(['descricao' => 'Pedido entregue']);
    }
}
