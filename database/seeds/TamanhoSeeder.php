<?php

use Illuminate\Database\Seeder;
use App\ProdutoTamanho;

class TamanhoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProdutoTamanho::create(['descricao' => 'PP']);
        ProdutoTamanho::create(['descricao' => 'P']);
        ProdutoTamanho::create(['descricao' => 'M']);
        ProdutoTamanho::create(['descricao' => 'G']);
        ProdutoTamanho::create(['descricao' => 'GG']);
        ProdutoTamanho::create(['descricao' => 'XGG']);
    }
}
