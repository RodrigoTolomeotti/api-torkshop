<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdutoPromocao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produto_promocao', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('id_produto');
            $table->foreign('id_produto')->references('id')->on('produto');
            $table->integer('desconto_porcentagem');
            $table->DateTime('data_inicio');
            $table->DateTime('data_fim');

            $table->DateTime('data_atualizacao');
            $table->DateTime('data_criacao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produto_promocao');
    }
}
