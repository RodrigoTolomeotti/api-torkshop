<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tables = [
          'tamanho',
          'cor',
          'modelo',
          'categoria',
          'tecido',
          'marca'
        ];

        foreach ($tables as $table) {
          Schema::rename($table, 'produto_' . $table);
        };
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      $tables = [
        'tamanho',
        'cor',
        'modelo',
        'categoria',
        'tecido',
        'marca'
      ];

      foreach ($tables as $table) {
        Schema::rename('produto_'.$table, $table);
      };
    }
}
