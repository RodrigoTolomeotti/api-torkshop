<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParceiro extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parceiro', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('nome', 45);
            $table->string('email', 45)->unique();
            $table->decimal('telefone', 15, 0);
            $table->decimal('celular', 15, 0);
            $table->string('cpf_cnpj', 15)->unique();
            $table->string('razao_social', 45)->nullable();
            $table->string('nome_fantasia', 80)->nullable();
            $table->decimal('cep', 8, 0);
            $table->string('endereco', 60);
            $table->decimal('numero', 10, 0);
            $table->string('complemento', 100)->nullable();
            $table->string('bairro', 30);
            $table->string('instagram_link', 30)->nullable();
            $table->string('facebook_link', 30)->nullable();

            $table->DateTime('data_atualizacao');
            $table->DateTime('data_criacao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parceiro');
    }
}
