<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('usuario', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('nome', 45);
            $table->string('sobrenome', 45);
            $table->date('data_nascimento');
            $table->decimal('saldo', 10, 2)->nullable();
            $table->string('email', 45)->unique();
            $table->string('cpf', 15)->unique();
            $table->string('rg', 15)->unique();
            $table->decimal('telefone', 15, 0)->nullable();
            $table->decimal('telefone_secundario', 15, 0)->nullable();
            $table->string('imagem', 30)->nullable();
            $table->bigInteger('id_usuario_referencia')->nullable();

            $table->DateTime('data_atualizacao');
            $table->DateTime('data_criacao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario');
    }
}
