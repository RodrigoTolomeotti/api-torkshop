<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnRgUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('usuario', function (Blueprint $table) {
          $table->string('rg', 15)->nullable(true)->change();;
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('usuario', function (Blueprint $table) {
          $table->string('rg', 15)->nullable(false)->change();;
      });
    }
}
