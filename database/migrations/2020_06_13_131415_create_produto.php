<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produto', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('nome', 45);
            $table->unsignedBigInteger('id_usuario'); // coluna que se refere a usuario.id
            $table->foreign('id_usuario')->references('id')->on('usuario'); // definição da referencia
            $table->unsignedBigInteger('id_tamanho');
            $table->foreign('id_tamanho')->references('id')->on('tamanho');
            $table->unsignedBigInteger('id_cor');
            $table->foreign('id_cor')->references('id')->on('cor');
            $table->unsignedBigInteger('id_modelo');
            $table->foreign('id_modelo')->references('id')->on('modelo');
            $table->unsignedBigInteger('id_categoria');
            $table->foreign('id_categoria')->references('id')->on('categoria');
            $table->unsignedBigInteger('id_tecido');
            $table->foreign('id_tecido')->references('id')->on('tecido');
            $table->unsignedBigInteger('id_marca');
            $table->foreign('id_marca')->references('id')->on('marca');
            $table->unsignedBigInteger('id_parceiro')->nullable();
            $table->foreign('id_parceiro')->references('id')->on('parceiro');
            $table->integer('id_produto_referencia')->nullable();
            $table->string('imagem_primaria', 256);
            $table->string('imagem_secundaria', 256);
            $table->text('descricao', 256);

            $table->date('data_atualizacao');
            $table->date('data_criacao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produto');
    }
}
