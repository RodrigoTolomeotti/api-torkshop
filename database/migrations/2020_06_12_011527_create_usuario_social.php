<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuarioSocial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario_social', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('id_usuario')->unique(); // coluna que se refere a usuario.id
            $table->foreign('id_usuario')->references('id')->on('usuario'); // definição da referencia
            $table->string('instagram_link', 30)->nullable();
            $table->string('facebook_link', 30)->nullable();
            $table->string('telegram_link', 30)->nullable();
            $table->string('twitter_link', 30)->nullable();

            $table->DateTime('data_atualizacao');
            $table->DateTime('data_criacao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario_social');
    }
}
