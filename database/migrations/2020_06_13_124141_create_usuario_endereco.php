<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuarioEndereco extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario_endereco', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('id_usuario')->unique(); // coluna que se refere a usuario.id
            $table->foreign('id_usuario')->references('id')->on('usuario'); // definição da referencia
            $table->string('cep', 8);
            $table->string('endereco', 60);
            $table->decimal('numero', 10, 0);
            $table->string('complemento', 60)->nullable();
            $table->string('bairro', 30);
            $table->boolean('endereco_principal')->nullable();

            $table->DateTime('data_atualizacao');
            $table->DateTime('data_criacao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario_endereco');
    }
}
