<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Usuario;
use App\Token;
use DateTime;

class LoginController extends Controller
{
    public function __construct() {
        $this->user = Auth::user();
    }

    public function login(Request $request) {
        try {
          $this->validate($request, [
              'email' => 'required|email',
              'password' => 'required|string|max:256'
          ]);

          $usuario = Usuario::where('email', $request->input('email'))->first();

          if (!$usuario || !Hash::check($request->input('password'), $usuario->password)) {
              return ['errors' => ['Credenciais inválidas']];
          }

          $token = Str::random(256);
          $usuario->tokens()->create([
              'token' => $token,
              'data_expiracao' => \DB::raw('NOW() + 1'),
              'ip' => $request->ip(),
              'user_agent' => $request->header('User-Agent')
          ]);

          return [
              'data' => $usuario,
              'token' => $token
          ];

        } catch (ValidationException $e) {
            $response = [];

            foreach ($e->errors() as $errors) {
                $response = array_merge($response, $errors);
            }

            return ['errors' => $response];
        }

    }

    public function getCurrentUser(Request $request)
    {

        return ['data' => Auth::user()];

    }

    private function getValidation() {

        return [
            'nome' => ['required','string','max:45'],
            'sobrenome' => ['required','string','max:45'],
            'data_nascimento' => ['required','date'],
            'email' => ['required','email', 'max:45', 'unique:usuario'],
            'cpf' => ['required','string','max:15', 'unique:usuario'],
            'rg' => ['nullable','string','max:15', 'unique:usuario'],
            'password' => ['required', 'string', 'max:40'],
        ];

    }

    public function create(Request $request) {

      try {

        $data = $this->validate($request, $this->getValidation());

        $data['password'] = Hash::make($data['password']);

        $usuario = Usuario::create($data);

        return['data' => 'Cadastrado com sucesso 👍'];

      } catch (ValidationException | Exception $e) {

        return['errors' => $e->errors()];

      }
    }
}
