<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    const CREATED_AT = 'data_criacao';
    const UPDATED_AT = 'data_atualizacao';

    public $table = 'usuario';

    protected $fillable = [
        'nome',
        'sobrenome',
        'endereco',
        'data_nascimento',
        'saldo',
        'email',
        'cpf',
        'rg',
        'telefone',
        'telefone_secundario',
        'imagem',
        'id_usuario_referencia',
        'password'
    ];

    public function tokens() {
        return $this->hasMany('App\Token');
    }

}
