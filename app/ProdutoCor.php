<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdutoCor extends Model
{
    const CREATED_AT = 'data_criacao';
    const UPDATED_AT = 'data_atualizacao';

    public $table = 'produto_cor';

    protected $fillable = [
        'nome',
        'codigo_html'
    ];

}
