<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdutoTamanho extends Model
{
    const CREATED_AT = 'data_criacao';
    const UPDATED_AT = 'data_atualizacao';

    public $table = 'produto_tamanho';

    protected $fillable = [
        'descricao'
    ];

}
